﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PersonneVoitureAPI.Controllers
{
    using Models;
    using Services;

    /// <summary>
    /// Controleur Personne
    /// Chemin d'accès : https://localhost:[port]/api/personne
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PersonneController : ControllerBase
    {
        private PersonneService service;

        public PersonneController(PersonneService service)
        {
            this.service = service;
        }

        //POST
        //Exemple Body JSON
        //{
        //    "nom": "dupont",
        //    "prenom": "pierre",
        //    "age": 30
        //}
        [HttpPost]
        [Route("")]
        public IActionResult Save([FromBody] Personne p)
        {
            return Ok(this.service.Sauvegarder(p));
        }

        //Afficher toutes les personnes : 
        //GET
        //https://localhost:[port]/api/personne
        [HttpGet]
        [Route("")]
        public IActionResult FindAll()
        {
            return Ok(this.service.TrouverTous());
        }

        //Exemple, afficher personne 1 : 
        //GET
        //https://localhost:[port]/api/personne/1
        [HttpGet]
        [Route("{id}")]
        public IActionResult FindById(int id)
        {
            try
            {
                return Ok(this.service.TrouverUn(id));
            }
            catch
            {
                return NotFound();
            }
        }

        //Exemple, changer personne 0 : 
        //PUT
        //https://localhost:[port]/api/personne/0
        //Exemple Body JSON
        //{
        //    "nom": "dupond",
        //    "prenom": "pierre",
        //    "age": 17
        //}
        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(int id, [FromBody] Personne p)
        {
            return Ok(this.service.Modifier(id, p));
        }

        //Exemple, supprimer personne 1 : 
        //DELETE
        //https://localhost:[port]/api/personne/1
        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                this.service.Supprimer(id);
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }
    }
}