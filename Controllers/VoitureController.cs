﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PersonneVoitureAPI.Controllers
{
    using Models;
    using Services;

    /// <summary>
    /// Controleur Voiture
    /// Chemin d'accès : https://localhost:[port]/api/voiture
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class VoitureController : ControllerBase
    {
        private VoitureService service;

        public VoitureController(VoitureService service)
        {
            this.service = service;
        }

        //POST
        //Exemple Body JSON
        //{
        //    "nom": "Model X",
        //    "marque": "Tesla",
        //    "immatriculation": "AB-123-CD",
        //    "proprietaireId": 0
        //}
        [HttpPost]
        [Route("")]
        public IActionResult Save([FromBody] Voiture v)
        {
            return Ok(this.service.Sauvegarder(v));
        }

        //Afficher toutes les voitures : 
        //GET
        //https://localhost:[port]/api/voiture
        [HttpGet]
        [Route("")]
        public IActionResult FindAll()
        {
            return Ok(this.service.TrouverTous());
        }

        //Exemple, afficher voiture 1 : 
        //GET
        //https://localhost:[port]/api/voiture/1
        [HttpGet]
        [Route("{id}")]
        public IActionResult FindById(int id)
        {
            try
            {              
                return Ok(this.service.TrouverUn(id));
            }
            catch
            {
                return NotFound();
            }
        }

        //Exemple, changer voiture 0 : 
        //PUT
        //https://localhost:[port]/api/voiture/0
        //Exemple Body JSON
        //{
        //    "nom": "Model X",
        //    "marque": "Tesla",
        //    "immatriculation": "AB-456-CD",
        //    "proprietaireId": 1
        //}
        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(int id, [FromBody] Voiture v)
        {
            return Ok(this.service.Modifier(id, v));
        }

        //Exemple, supprimer voiture 1 : 
        //DELETE
        //https://localhost:[port]/api/voiture/1
        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                this.service.Supprimer(id);
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }
    }
}