﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Models
{
    /// <summary>
    /// Entité Personne
    /// Contient : Id, Nom, Prenom, Age.
    /// </summary>
    public class Personne
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }
        public List<Voiture> Voitures { get; set; }

        public Personne()
        {
            Voitures = new List<Voiture>();
        }
    }

    
}
