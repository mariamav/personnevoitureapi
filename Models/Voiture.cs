﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Models
{
    /// <summary>
    /// Entité Voiture
    /// Contient : Id, Nom, Marque, Immatriculation, Propriétaire.
    /// </summary>
    public class Voiture
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Marque { get; set; }
        public string Immatriculation { get; set; }
        //public Personne Proprietaire { get; set; }
        public int ProprietaireId { get; set; }
    }
}
