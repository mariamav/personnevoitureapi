# Creation d'une application backend

Échéance : 22 mars 2020 12:00

## Instructions

Le but de ce devoir est de faire un backend en C# avec ASP.NET et MVC.NET et le livrer sur un Git distant Publique.

    1. Créer un nouveau depo git public
    2. Créer un nouveau projet .NET

- Il consiste en deux CRUD de voiture et personne.

- Une voiture possède:
  - un nom
  - une marque
  - une immatriculation
  - un proprietaire
  
- Une personne possède:
  - un nom
  - un prenom
  - un age

- Il y aura deux Controllers (VoitureController et PersonneController).
- Il y aura deux services.
- Il y aura deux repositories.
- Ils sont reliés par IOD (injection de dependance).
- Les repositories peuvent etre des bouchons (utilisation de liste).

En Bonus:

- Retourner la liste des voitures d'un utilisateur.
- N'ajouter la voiture uniquement si le propriétaire est majeur.
- Connecter à une Base de données.
- Changer le Repository pour qu'il lance les requetes grace à Entity.
- Faire le retour des status et la gestion des erreurs.
- Documenter votre code.
