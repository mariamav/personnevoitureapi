﻿using PersonneVoitureAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Repositories.Impl
{
    /// <summary>
    /// Implémentation de PersonneRepo.
    /// </summary>
    public class PersonneListRepo : PersonneRepo
    {
        private List<Personne> personnes = new List<Personne>();

        //Ajouter une personne à la liste personnes.
        public Personne Save(Personne p)
        {
            p.Id = this.personnes.Count();
            this.personnes.Add(p);
            return p;
        }

        //Retourner la liste des personnes enregitrées.
        public IEnumerable<Personne> FindAll()
        {
            return this.personnes.Where(p => p != null);
        }

        //Retourner la personne portant l'id donné.
        public Personne FindById(int id)
        {
            return this.personnes[id];
        }

        //Mettre à jour la personne portant l'id donné.
        public Personne Update(Personne p)
        {
            this.personnes[p.Id] = p;
            return p;
        }

        //Supprimer la voiture de la liste Voitures, si existe, afin de changer son propriétaire lors de la MAJ.
        public void UpdateCar(Voiture v)
        {
            foreach(Personne personne in personnes)
            {
                foreach(Voiture voiture in personne.Voitures.ToArray())
                {
                    if(voiture.Id == v.Id)
                    {
                        personne.Voitures.Remove(voiture);
                    }
                }
            }
            
        }

        //Supprimer la personne portant l'id donné.
        //Mise à null pour garder les id dans le bon ordre.
        public void Delete(int id)
        {
            this.personnes[id] = null;
        }

        //Supprimer une voiture de la liste des Voitures d'une personne.
        public void DeleteCar(int id)
        {
            foreach (Personne personne in personnes)
            {
                foreach (Voiture voiture in personne.Voitures.ToArray())
                {
                    if (voiture.Id == id)
                    {
                        personne.Voitures.Remove(voiture);
                    }
                }
            }
        }

        //Supprimer la personne donnée.
        //Mise à null pour garder les id dans le bon ordre.
        public void Delete(Personne p)
        {
            this.personnes[p.Id] = null;
        }
    }
}
