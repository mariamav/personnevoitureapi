﻿using PersonneVoitureAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Repositories.Impl
{
    /// <summary>
    /// Implementation de VoitureRepo.
    /// </summary>
    public class VoitureListRepo : VoitureRepo
    {
        private List<Voiture> voitures = new List<Voiture>();
        
        //Ajouter une voiture à la liste voitures.
        public Voiture Save(Voiture v)
        {
            v.Id = this.voitures.Count();
            this.voitures.Add(v);
            return v;
        }

        //Retourner la liste des voitures enregitrées.
        public IEnumerable<Voiture> FindAll()
        {
            return this.voitures.Where(v => v != null);
        }

        //Retourner la voiture portant l'id donné.
        public Voiture FindById(int id)
        {
            return this.voitures[id];
        }

        //Mettre à jour la voiture portant l'id donné.
        public Voiture Update(Voiture v)
        {
            this.voitures[v.Id] = v;
            return v;
        }

        //Supprimer la voiture portant l'id donné.
        //Mise à null pour garder les id dans le bon ordre.
        public void Delete(int id)
        {
            this.voitures[id] = null;
        }

        //Supprimer la voiture donnée.
        //Mise à null pour garder les id dans le bon ordre.
        public void Delete(Voiture v)
        {
            this.voitures[v.Id] = null;
        }
    }
}
