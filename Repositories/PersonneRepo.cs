﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Repositories
{
    using Models;
    public interface PersonneRepo
    {
        public Personne Save(Personne p);
        public IEnumerable<Personne> FindAll();
        public Personne FindById(int id);
        public Personne Update(Personne p);
        public void UpdateCar(Voiture v);
        public void Delete(int id);
        public void DeleteCar(int id);
        public void Delete(Personne p);
    }
}
