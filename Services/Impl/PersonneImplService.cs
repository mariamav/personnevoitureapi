﻿using PersonneVoitureAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Services.Impl
{
    using Repositories;
    /// <summary>
    /// Implémentation de PersonneService.
    /// </summary>
    public class PersonneImplService : PersonneService
    {
        private PersonneRepo repo;

        public PersonneImplService(PersonneRepo repo)
        {
            this.repo = repo;
        }

        public Personne Sauvegarder(Personne p)
        {
            return this.repo.Save(p);
        }

        public IEnumerable<Personne> TrouverTous()
        {
            return this.repo.FindAll();
        }

        public Personne TrouverUn(int id)
        {
            return this.repo.FindById(id);
        }

        public Personne Modifier(int id, Personne p)
        {
            p.Id = id;
            return this.repo.Update(p);
        }

        public void ModifierVoiture(Voiture v)
        {
            this.repo.UpdateCar(v);
        }

        public void Supprimer(int id)
        {
            this.repo.Delete(id);
        }

        public void SupprimerVoiture(int id)
        {
            this.repo.DeleteCar(id);
        }
    }
}
