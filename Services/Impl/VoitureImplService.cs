﻿using PersonneVoitureAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Services.Impl
{
    using Repositories;
    /// <summary>
    /// Implémentation de VoitureService.
    /// </summary>
    public class VoitureImplService : VoitureService
    {
        private VoitureRepo repo;
        private PersonneService persServ;

        public VoitureImplService(VoitureRepo repo, PersonneService persServ)
        {
            this.repo = repo;
            this.persServ = persServ;
        }

        public Voiture Sauvegarder(Voiture v)
        {
            //Version précédente,
            //permet d'afficher les infos du propriétaire,
            //boucle infinie, si on donne au propriétaire une liste de voitures.
            /*v.Proprietaire = this.persServ.TrouverUn(v.Proprietaire.Id);
            if (v.Proprietaire.Age >= 18)
            {
                return this.repo.Save(v);
            }
            return null;*/
            

            //N'affiche que l'id du propriétaire,
            //mais donne la possibilité d'ajouter la voiture à la liste des Voitures d'une personne.
            Personne p = this.persServ.TrouverUn(v.ProprietaireId);
            //La voiture ne peut être enregistrée que si son propriétaire est majeur.
            if (p.Age >= 18)
            {
                p.Voitures.Add(v);
                return this.repo.Save(v);
            }
            return null;
        }

        public IEnumerable<Voiture> TrouverTous()
        {
            return this.repo.FindAll();
        }

        public Voiture TrouverUn(int id)
        {
            return this.repo.FindById(id);
        }

        public Voiture Modifier(int id, Voiture v)
        {
            v.Id = id;
            /*v.Proprietaire = this.persServ.TrouverUn(v.Proprietaire.Id);*/
            
            Personne p = this.persServ.TrouverUn(v.ProprietaireId);

            if (p.Age >= 18)
            {
                //Si la voiture appartient à quelqu'un, elle est retirée de la liste des Voitures de cette personne.
                //Puis ajoutée à la liste des Voitures de la personne indiquée.
                //Si le propriétaire donné n'est pas majeur, la voiture garde son ancien propriétaire si existe.
                this.persServ.ModifierVoiture(v);
                p.Voitures.Add(v);
                return this.repo.Update(v);
            }
            return null;
        }

        public void Supprimer(int id)
        {
            this.persServ.SupprimerVoiture(id);
            this.repo.Delete(id);
        }
    }
}
