﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Services
{
    using Models;
    public interface PersonneService
    {
        public Personne Sauvegarder(Personne p);
        public IEnumerable<Personne> TrouverTous();
        public Personne TrouverUn(int id);
        public Personne Modifier(int id, Personne p);
        public void ModifierVoiture(Voiture v);
        public void Supprimer(int id);
        public void SupprimerVoiture(int id);
    }
}
