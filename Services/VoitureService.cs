﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonneVoitureAPI.Services
{
    using Models;
    public interface VoitureService
    {
        public Voiture Sauvegarder(Voiture v);
        public IEnumerable<Voiture> TrouverTous();
        public Voiture TrouverUn(int id);
        public Voiture Modifier(int id, Voiture v);
        public void Supprimer(int id);
    }
}
